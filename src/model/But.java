/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Random;

/**
 *
 * @author djulhya
 */
public class But implements Serializable{
    private int but;
    public But() {initBut(0);}

    public But(int b) {initBut(b);}

    public But(Config c) {butConfig(c);}
    
    public void initBut(int but) {this.but = but;}
    
    public int getBut(){return this.but;}
    
    private void butConfig(Config c) {
        Random r = new Random();
        switch (c) {
            case CONF2:
                //but inferieur à 500 et en nombrepaire
                do{this.but = r.nextInt(900)+100;}
                while(but%2!=0 || but > 500);
                break;
            case CONF3:
                //but superieur à 500 et en nombre impaire
                do{this.but = r.nextInt(900)+100;}
                while(but%2==0 || but < 500);  
                break;
            case CONF1:
            case DEFAULT:
                this.but = r.nextInt(900)+100;;
        }
    }
    /*
    private int configuration(int conf)
    {
        boolean res = false;
        int x = 0;
        
        while(res==false)
        {
            Random r = new Random();
            x = r.nextInt(900)+100;
            if(conf==0)
                res=true;
            else if(conf==1)
            {
                if(x%2==0 && x < 500)
                    res=true;
            }
            else if(conf==2)
            {
                if(x%2!=0 && x > 500)
                    res=true;
            }
            
        }
        
        return x;
    }
*/
    
}
