/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author djulhya
 */
public class Solution implements Serializable{
    
    private List<Expression> ListExpression = new ArrayList<>();
    
    public Solution(){}
    
    public void addExpression(Expression e)
    {
        this.ListExpression.add(e);
    }
    
    public void addExpressionAdaptateur(AdaptateurExpression ae){
        this.ListExpression.add(ae.getExpresion());
    }
    
    public List<Expression> getListExpression()
    {
        return this.ListExpression;
    }
    
    public void initSolution()
    {
        this.ListExpression.clear();
    }
    
    public int getResultat()
    {
        int x = 0;
        if(!this.ListExpression.isEmpty())
            x =this.ListExpression.get(
                    this.ListExpression.size()-1).getTotal();
        
        return x;
    }
    
    public String toString(){
        String str="";
        for(Expression e:ListExpression)
        {
            str+=e.toString()+"\n";
        }
        return str;
    }
}
