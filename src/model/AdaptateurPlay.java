/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 2605glmiandambuyi
 */
public class AdaptateurPlay {
    
    GameLib game;
   public  AdaptateurPlay(GameLib g)
    {
        game = g;
    }
    
    public int getBut()
    {
        return this.game.goal();
    }
    
    public List<Plaques> getListPlaque()
    {
        List<Plaques> lPlaques = new ArrayList<>();
        
        for(PlateLib p : this.game.gamePlates())
        {
            Plaques pl = new Plaques(p.value());
            pl.SetSlection(true);
            lPlaques.add(pl);
        }
        
        
        return lPlaques;
    }
   
}
