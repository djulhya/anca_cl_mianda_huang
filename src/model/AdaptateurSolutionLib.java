/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.ExpressionLib;
import interfaces.SolutionLib;
import java.util.List;

/**
 *
 * @author localwsp
 */
public class AdaptateurSolutionLib implements SolutionLib{

     private Solution sol;
    
    public AdaptateurSolutionLib(Solution solution){
        sol=solution;
    }

    @Override
    public List<ExpressionLib> getExpressions() {
        List<ExpressionLib> listExpressionLib=null;
        for (Expression e:sol.getListExpression()){
            listExpressionLib.add(new AdaptateurExpressionLib(e));
        }
        return listExpressionLib;
    }

    @Override
    public int getResult() {
        return sol.getResultat();
    }

    @Override
    public int compareTo(Object t) {
        //comparere la solution => comparer d'expression en experssion
        SolutionLib sl;
        /*if(t instanceof SolutionLib){
            sl = (SolutionLib)t;
            
            for (Expression e:sol.getListExpression()){
            listExpressionLib.add(new AdaptateurExpressionLib(e));
            }   
        }*/
        
        return -1;
    }
     
}
