/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author djulhya
 */
public class ListPlaques implements Serializable{
    
    List<Plaques> listPlaques = new ArrayList<>();
    List<Plaques> listPlaqueSelect = new ArrayList<>();
    
    public ListPlaques(List<Plaques> lp)
    {
        confDefaut();
        this.listPlaqueSelect = lp;
    }
    public ListPlaques(int conf)
    {
        switch(conf){
            case 0:
                this.confDefaut();
                break;
            case 1:
                this.confFacile();
                break;
            case 2:
                this.confDifficile();
                break;
        }
        
        initListPlaqueSelect();
    }
    
    ListPlaques(Config c) {
        switch(c)
        {
                case CONF2:
                    this.confFacile();break;
                case CONF3:
                    this.confDifficile(); break;
                case CONF1:
                case DEFAULT:
                    this.confDefaut(); break;
        }
        
        initListPlaqueSelect();
    }
    
    
    private void confFacile()
    {
        //plaque paire
        for(int i = 0 ; i<2; i++)
            for(int j = 1 ; j<6 ; j++)
            {
                Plaques p = new Plaques(j*2);
                this.listPlaques.add(p);
            }
        int y = 25;
        for(int x = 0 ; x<2 ; x++)
        {
            Plaques p = new Plaques(y*2);
            this.listPlaques.add(p);
            y = y*2;
        }
    }
    
    private void confDifficile()
    {
        //pas de doublon
        for(int j = 0 ; j<10 ; j++)
        {
            Plaques p = new Plaques(j+1);
            this.listPlaques.add(p);
        }
            
        int y = 25;
        for(int x = 0 ; x<4 ; x++)
        {
            Plaques p = new Plaques(y);
            this.listPlaques.add(p);
            y = y+25;
        }
    }
    
    private void confDefaut()
    {
        for(int i = 0 ; i<2; i++)
            for(int j = 0 ; j<10 ; j++)
            {
                Plaques p = new Plaques(j+1);
                this.listPlaques.add(p);
            }
        int y = 25;
        for(int x = 0 ; x<4 ; x++)
        {
            Plaques p = new Plaques(y);
            this.listPlaques.add(p);
            y = y+25;
        }
    }
    
    private void initListPlaque()
    {
        for(Plaques p : this.listPlaques)
            p.SetSlection(false);
    }
    
    public void initListPlaqueSelect()
    {
        this.listPlaqueSelect.clear();
        initListPlaque();
        
        int x = 0;
        while(x<6)    
        {
            Random r = new Random();
            Plaques p = this.listPlaques.get(r.nextInt(this.listPlaques.size())) ;
            if(p.getSelection()==false)
            {
                p.SetSlection(true);
                this.listPlaqueSelect.add(new Plaques(p.getNombre()));
                ++x;
            }
        }
    }
    
    public List<Plaques> getListPlaqueSelect()
    {
        return this.listPlaqueSelect;
    }
    
    public boolean EstUnePlaque(int x)
    {
        boolean res = false;
        for(Plaques p : this.listPlaqueSelect)
        {
            if(p.getNombre() == x)
                res = true;
        }
        
        return res;
        
    }
    
    public void ajouterPlaque(int x)
    {
        Plaques p = new Plaques(x);
        p.SetSlection(true);
        this.listPlaqueSelect.add(p);
        
    }
    
    public boolean doublePlaque(int x )
    {
        boolean res = false;
        int y = 0;
        for(Plaques p : this.listPlaqueSelect)
        {
            if(p.getNombre()== x)
                ++y;
        }
        if(y>1)
        {
            res = true;
        }
        
        return res;
    }
    
    public void retirerPlaque(int x)
    {
        boolean res = false;
        for(int i = 0 ; i < this.listPlaqueSelect.size()
                && res == false ; ++i)
        {
            Plaques p = this.listPlaqueSelect.get(i);
            if(p.getNombre() == x)
            {
                this.listPlaqueSelect.remove(p);
                res = true;
            }
        }         
       
    }
    
    public String toString()
    {
        String str ="";
        for(Plaques p : this.listPlaqueSelect)
            str +="  "+p.getNombre();
        
        return str;
    }
    
}
