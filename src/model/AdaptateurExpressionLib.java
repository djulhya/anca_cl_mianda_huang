/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import interfaces.ExpressionLib;

/**
 *
 * @author xun
 */
public class AdaptateurExpressionLib implements ExpressionLib{
    
    private Expression ex;
    
    public AdaptateurExpressionLib(Expression expression){
        ex = expression;
    }

    @Override
    public int value() {
        return ex.getTotal();
    }

    @Override
    public boolean isUnary() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int leftOperand() {
        return ex.getGauche();
    }

    @Override
    public int rightOperand() {
        return ex.getDroite();
    }

    @Override
    public String operator() {
        return ex.getOperation();
    }
    
}
