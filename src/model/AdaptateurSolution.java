/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.ExpressionLib;
import interfaces.SolutionLib;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author localwsp
 */
public class AdaptateurSolution extends Solution{
    

    private Solution sol;

    
    public AdaptateurSolution(){}
    
    public List<Solution> getListSolution(List<SolutionLib> lsl){
        List<Solution> listSolution=new ArrayList<>(); 
        for(SolutionLib sl : lsl)
        {
            //list de solution add une nouvelle solution
            listSolution.add(getSolution(sl));
        }
        return listSolution;
    }
    
    public Solution getSolution(SolutionLib slc){
        
        sol = new Solution();
        for (ExpressionLib el: slc.getExpressions()){
            AdaptateurExpression ae = new AdaptateurExpression(el);
            sol.addExpressionAdaptateur(ae);
        }
        return sol;
    
    }
    
    
}
