/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import db.dbConnect;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author Eurosab
 */
public class Joueur implements Serializable{
    

    int numJoeur;
    String idJoueur="";
    String motDePasse ="";
    public int msg =-1; 
    Config config=Config.DEFAULT;
    private Stack<Play> listPlay = new Stack();
    private long lastSessionTime=0;
    
    public Joueur()
    {
        
        this.config=Config.DEFAULT;
        this.numJoeur = -1;
        this.lastSessionTime = Calendar.getInstance().getTimeInMillis();
    }
    
    public Joueur(String pseudo , String mdp)
    {
        this.config=Config.DEFAULT;
        this.numJoeur = -1;
        this.lastSessionTime = Calendar.getInstance().getTimeInMillis();
        this.idJoueur = pseudo;
        this.motDePasse = mdp;
    }
    
    
    public synchronized void setConfiguration(int conf)
    {
        
        config=Config.values()[conf];
    }
    
    public synchronized void setNumJoueur(int num)
    {
        this.numJoeur = num;
    }
    
    public synchronized int getNumJoueur()
    {
        return this.numJoeur;
    }
    
    public String getIdJoueur()
    {
        return this.idJoueur;
    }
    public void setIdJoueur(String ps)
    {
        this.idJoueur = ps;
    }
    
    public String getMotDePasse()
    {
        return this.motDePasse;
    }
    public void setMotDePasse(String mdp)
    {
        this.motDePasse = mdp;
    }
    
    public synchronized void ajouterPlay(Play p)
    {
        this.listPlay.push(p);
    }
    
    public synchronized Stack<Play> getListPlay()
    {
        return this.listPlay;
    }
    
    public synchronized Play getPlay()
    {
        return this.listPlay.peek();
    }
    
    public synchronized void setPlay(Play p)
    {
        if(!this.listPlay.isEmpty())
        {
            this.listPlay.peek().setMessage(p.getMessage());
            this.listPlay.peek().setScore(p.getScore());
            this.listPlay.peek().setSolution(p.getSolution());
            this.listPlay.peek().setButPropose(p.getButPropose());
            this.listPlay.peek().setJouer();
        }
        
        
    }
    
    public boolean dernierJeuJouer()
    {
        boolean b = false;
        if(!this.listPlay.isEmpty())
            b = this.listPlay.peek().getJouer().equals("ok");
        return b;
    }
    
    public synchronized void supprimerDernierJeu()
    {
        if(!this.listPlay.isEmpty())
            this.listPlay.pop();
    }
    public synchronized int nbrJeu()
    {
        return this.listPlay.size();
    }

    public synchronized Config getConfig(){return this.config;}
    
    public synchronized void setLastSessionTime()
    {this.lastSessionTime = Calendar.getInstance().getTimeInMillis();}
    
    public synchronized long getLastSessionTime(){return this.lastSessionTime;}

    public List<Solution> getBestSolution() {
        List<Solution> bestSol=new ArrayList<>();
        if(this.listPlay.size()!=0){
            Play p=this.listPlay.peek();
            List<Solution> sl = p.bestSolutions();
        
            for(int i=0; i<3; ++i){
                bestSol.add(sl.get(i));
            }
        
        }
        return bestSol;
    }


//----------------  Debut connect DB

    public List<String> getListPlayConfig(int config) {
      List <String> l= new ArrayList<>();
      dbConnect connect= new dbConnect();
      connect.connexion();
      if(connect.existJoueur(this.idJoueur, this.motDePasse)){
          l= connect.getPartiesConfig(idJoueur,config);
      }
      else{
          l= null;
      }
      connect.deconnexion();
      return l;
    }

    public Object getListPlayIdSession() {
        List <String> l= new ArrayList<>();
        dbConnect connect= new dbConnect();
        connect.connexion();
        if(connect.existJoueur(this.idJoueur, this.motDePasse)){
          l= connect.getPartiesIdSession(this.msg);
        }
        else{
            l= null;
        }
        connect.deconnexion();
        return l;
    }

    public List<String> getListPlaySessionDate() {
        List <String> l= new ArrayList<>();
        dbConnect connect= new dbConnect();
        connect.connexion();
        if(connect.existJoueur(this.idJoueur, this.motDePasse)){
          l= connect.getListPlaySessionDate(this.idJoueur);
        }
        else{
            l= null;
        }
        connect.deconnexion();
        return l;   
    
    }

    public boolean inscrire() {
        dbConnect connect= new dbConnect();
        connect.connexion();
        boolean retour = false;
        if(connect.existJoueur(this.idJoueur, this.motDePasse)){
        }
        else{
            retour = connect.addJoueur(idJoueur, idJoueur);
        }
        connect.deconnexion();
        return retour;
    }

    public boolean existJoueur() {
        dbConnect connect= new dbConnect();
        connect.connexion();
        boolean retour = false;
        if(connect.existJoueur(this.idJoueur, this.motDePasse)){
            retour = true;
        }
        connect.deconnexion();
        return retour;
    }

    public void addListPlay() {
        
        int lastSession = 0;
        if(this.listPlay.size()!=0)
        {
            dbConnect connect= new dbConnect();
            connect.connexion();
            for(Play p: listPlay)
            {
                if(!connect.existPartie(p.getNumJeu()))
                {
                    if(connect.existSession(this.idJoueur)){    
                         
                        lastSession = connect.getLastIdSession(idJoueur);
                        System.out.println("lastSession = "+lastSession);
                    }
                    else
                    {
                        connect.addSession(this.idJoueur);
                    }
                    boolean b=connect.addPartie(p,lastSession);
                }
            }
            connect.deconnexion();
        }
    
    }

}
