/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Eurosab
 */
public class Expression implements Serializable{
    
    private int op1;
    private int op2;
    private int total;
    private Operation operation;
    
    public Expression(){}
    public Expression(int x , int y , int z , Operation op)
    {
        this.op1 = x;
        this.op2 = y;
        this.total = z;
        this.operation = op;
    }
    
    public Expression(int x , int y , int z , String s)
    {
        this.op1 = x;
        this.op2 = y;
        this.total = z;
        this.operation = getOperator(s);
    }
    
    public boolean verifierExpression()
    {
        return (this.total == this.calcul());
    }
    
    //il faut remettre le calcul en private
    public int calcul()
    {       
        int res = -1;
        if(this.operation==Operation.addition) res=this.op1+this.op2;
        else if(this.operation==Operation.multiplication) 
            res=this.op1*this.op2;
        else if(this.operation==Operation.soustraction 
                && this.op1>=this.op2) res= this.op1-this.op2;
        else if(this.operation==Operation.division && this.op2 != 0 && 
                (this.op1%this.op2==0)) res=this.op1/this.op2;
        else
            res=-1;
        return res;
    }
    
    public int getTotal()
    {
        return this.total;
    }
    
  public int getGauche(){return this.op1;}
  
  public int getDroite(){return this.op2;}
  
  public String getOperation(){
      String s="";
      if(this.operation==Operation.addition) s="+";
        else if(this.operation==Operation.multiplication) s="*";
        else if(this.operation==Operation.soustraction) s="-";
        else if(this.operation==Operation.division) s="/";
        else ;
      return s;
      
  }
  
    @Override
  public String toString()
  {
      String s="";
      s+=this.op1+" "+this.getOperation()+" "+this.op2+" = "+this.total;
      return s;
  }
    
    private Operation getOperator(String s) {
        Operation o=null;
        switch(s){
        case "+": o = Operation.addition;break;
        case "-": o = Operation.soustraction;break;
        case "*": o = Operation.multiplication;break;
        case "/": o = Operation.division;break;
        }
        return o;
    }
}
