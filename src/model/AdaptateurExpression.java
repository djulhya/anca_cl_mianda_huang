/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.ExpressionLib;
import java.io.Serializable;

/**
 *
 * @author localwsp
 */
public class AdaptateurExpression extends Expression{
    
    private Expression ex;


    public AdaptateurExpression(ExpressionLib elb){
        ex = new Expression(elb.leftOperand(),elb.rightOperand(), elb.value(),elb.operator());
    }
    
    public Expression getExpresion(){
        return this.ex;
    }

}
