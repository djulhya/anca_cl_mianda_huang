/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.PlateLib;

/**
 *
 * @author 2605glmiandambuyi
 */
public class AdaptateurPlateLib implements PlateLib{

    Plaques pl;
    public AdaptateurPlateLib(Plaques p)
    {
        this.pl = p;
    }
    @Override
    public int value() {
        return this.pl.getNombre();
    }
    
}
