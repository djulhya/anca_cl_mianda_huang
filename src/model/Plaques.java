/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author djulhya
 */
public class Plaques implements Serializable{
    
    private int nombre;
    private boolean selection;
    
    public Plaques(int i)
    {
        this.nombre = i;
        this.selection = false;
    }
    
    public void SetSlection(boolean b)
    {
        this.selection = b;
    }
    
    public int getNombre()
    {
        return this.nombre;
    }
    
    public boolean getSelection()
    {
        return this.selection;
    }

}
