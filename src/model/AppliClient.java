/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import commun.ExceptionJeu;
import commun.Message;
import commun.Protocol;
import java.util.List;
import java.util.Stack;
import protocol.ProtocolClient;

/**
 *
 * @author djulhya
 */
public class AppliClient {
    ProtocolClient prtClient;  
    Message msg;
    
   
    public AppliClient(String adrIP, int port) 
    {
        prtClient = new ProtocolClient(adrIP, port);
        this.msg = new Message(0,null);
    }
    
    public int setConfiguration(int conf , Joueur j) throws ExceptionJeu
    {
        
        switch (conf)
        {
            case 0:
                this.msg = this.prtClient.send_message(Message.OP_CONFIG_DEFAULT, j);
                break;
            case 1 :
                this.msg = this.prtClient.send_message(Message.OP_CONFIG_FACILE, j);
                break;
            case 2:
                this.msg = this.prtClient.send_message(Message.OP_CONFIG_MOYEN, j);
                break;
            case 3 :
                this.msg = this.prtClient.send_message(Message.OP_CONFIG_DIFFICILE, j);
                break;
            case 4 :
                this.msg = this.prtClient.send_message(Message.OP_CONFIG_NO_SOLUTION, j);
                break;
            case 5 : 
                this.msg = this.prtClient.send_message(Message.OP_CONFIG_SOLUTION, j);
                break;
            default:
                this.msg = this.prtClient.send_message(Message.OP_CONFIG_DEFAULT, j);
                break;
        }
        
        if (this.msg.getCode()== Message.SESSION_FERME)
        {
            System.out.println("la session est fermé. veuillez choisir une des options suivantes");
            return -1;
        }
        else if (traiter_rep(msg))
            return (int)this.msg.getObject();
        else
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
    }
    
    public int creerJoeur(int conf)throws ExceptionJeu
    {
        this.msg = this.prtClient.send_message(Message.OP_NOUVEAU_JOEUR, conf);
        if (traiter_rep(msg))
            return (int)this.msg.getObject();
        else
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
    }
    
    public int inscrireJoueur(Joueur j)throws ExceptionJeu
    {
        this.msg = this.prtClient.send_message(Message.OP_INSCRIPTION, j);
        if (traiter_rep(msg))
            return (int)this.msg.getObject();
        else
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
    }
    
    public int connecterJoueur(Joueur j)throws ExceptionJeu
    {
        this.msg = this.prtClient.send_message(Message.OP_CONNEXION, j);
        if (traiter_rep(msg))
            return (int)this.msg.getObject();
        else
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
    }
    
    public Object lancerJeu(Joueur j) throws ExceptionJeu
    {
        this.msg=this.prtClient.send_message(Message.OP_DEB, j);
        if (this.msg.getCode()== Message.SESSION_FERME)
        {
            System.out.println("la session est fermé. veuillez choisir une des options suivantes");
            return -1;
        }
        else if (traiter_rep(msg))
        {
            return (Play)this.msg.getObject();
        }
        else
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);

    }
    
    public Integer finJeu(Object o) throws ExceptionJeu
    {
        this.msg = this.prtClient.send_message(Message.OP_FIN, o);
        
        if(this.msg.getCode()==Message.ERREUR_EXPRESSION)
            System.out.println("reponse serveur : "+"erreur expression");
        else if(this.msg.getCode()==Message.MAUVAISE_PLAQUE)
            System.out.println("reponse serveur : "+"mauvais choix de plaques");
        else if(this.msg.getCode()==Message.TIMEOUT)
            System.out.println("reponse serveur : "+"temps dépassé");
        else if(this.msg.getCode()==Message.MAUVAIS_JEU)
            System.out.println("reponse serveur : "+"Jeu inéxistant");
        
        return (Integer) this.msg.getObject();
    }
    
    public void quitterJeu(Joueur j) throws ExceptionJeu {
        
        this.msg = this.prtClient.send_message(Message.OP_DEC, j);
        if (traiter_rep(msg));
   }
  
    public Object afficherPlay(Joueur j)throws ExceptionJeu
    {
        Stack<Play> st = null;
        this.msg = this.prtClient.send_message(Message.LISTE_PLAY,j);
        if (this.msg.getCode()== Message.SESSION_FERME)
        {
            
            return -1;
        }
        else
        if (traiter_rep(msg))
            return (Stack<Play>)this.msg.getObject();
        else
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
       
    }
    
    public Object afficherSession(Joueur j)throws ExceptionJeu
    {
        List<String> st = null;
        this.msg = this.prtClient.send_message(Message.LISTE_SESSION_DATE,j);
        if (this.msg.getCode()== Message.SESSION_FERME)
        {
            
            return -1;
        }
        else
        if (traiter_rep(msg))
            return (List<String>)this.msg.getObject();
        else
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
       
    }
    
    public Object afficherPlayDate(Joueur j)throws ExceptionJeu
    {
        this.msg = this.prtClient.send_message(Message.LISTE_PLAY_DATE,j);
        if (this.msg.getCode()== Message.SESSION_FERME)
        {
            
            return -1;
        }
        else
        if (traiter_rep(msg))
            return (List<String>)this.msg.getObject();
        else
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
       
    }
    
    public Object afficherPlayConf(int conf , Joueur j)throws ExceptionJeu
    {
        Stack<Play> st = null;
        
        switch (conf)
        {
            case 1:
                this.msg = this.prtClient.send_message(Message.LISTE_PLAY_CONF1, j);
                break;
            case 2 :
                this.msg = this.prtClient.send_message(Message.LISTE_PLAY_CONF2, j);
                break;
            case 3:
                this.msg = this.prtClient.send_message(Message.LISTE_PLAY_CONF3, j);
                break;
            case 4 :
                this.msg = this.prtClient.send_message(Message.LISTE_PLAY_CONF4, j);
                break;
            case 5 :
                this.msg = this.prtClient.send_message(Message.LISTE_PLAY_CONF5, j);
                break;
            case 6 : 
                this.msg = this.prtClient.send_message(Message.LISTE_PLAY_CONF6, j);
                break;
            default:
                this.msg = this.prtClient.send_message(Message.LISTE_PLAY_CONF1, j);
                break;
        } 
        
        
        if (this.msg.getCode()== Message.SESSION_FERME)
        {
            System.out.println("la session est fermé. veuillez choisir une des options suivantes");
            return -1;
        }
        else
        if (traiter_rep(msg))
            return (List<String>)this.msg.getObject();
        else
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
       
    }
    
    public Object afficherBestSolution(Joueur j)throws Exception
    {
        this.msg = this.prtClient.send_message(Message.BEST_SOLUTION,j);
        if (this.msg.getCode()== Message.SESSION_FERME)
        {
            System.out.println("la session est fermé. veuillez choisir une des options suivantes");
            return -1;
        }
        else
        if (traiter_rep(msg)){
            return (List<Solution>)this.msg.getObject();
        }
        else
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
            
    }       
            
    private boolean traiter_rep(Message reponse) throws ExceptionJeu {
        if (reponse == null) 
        {
            throw new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
        }
        if (reponse.getCode() == Message.M_OK) return true;
        throw traiter_err(reponse.getCode());
    }

    private ExceptionJeu traiter_err(int err) {
        switch (err) {
            case Protocol.CONN_KO:
                return new ExceptionJeu(ExceptionJeu.typeErreur.CONN_KO);
            default:
                return new ExceptionJeu(ExceptionJeu.typeErreur.AUTRE);
        }
    }
        
}
