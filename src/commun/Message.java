/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package commun;

import java.io.Serializable;

/**
 *
 * @author xun
 */
public class Message implements Serializable{
    
    private Integer code = 0;
    private Object o;
   
    //Messages de retour
    public static final int M_OK = 1; //Pas de probleme
    public static final int M_KO = 2; //KO raison inconnue
    
    //Messages d'opérations jeu
    
    public static final int OP_DEB = 20; //debut du jeu
    public static final int OP_FIN = 30; //fin du jeu
     
    //Messages d'opérations joueur
    
    public static final int OP_NOUVEAU_JOEUR = 80; // nouveau joueur anonyme
    public static final int OP_INSCRIPTION = 81; // inscription joueur
    public static final int OP_CONNEXION = 82;// connexion joueur inscrit
    public static final int OP_DEC = 40; //Déconnexion
    
    //Messages d'opérations configuration
    
    public static final int OP_CONFIG_DEFAULT = 90; //choix configuration par defaut
    public static final int OP_CONFIG_FACILE = 100; //choix configuration facile
    public static final int OP_CONFIG_DIFFICILE = 110; //choix configuration dificile
    public static final int OP_CONFIG_MOYEN = 111; //choix configuration moyenne
    public static final int OP_CONFIG_SOLUTION = 112; //choix configuration avec solution exacte
    public static final int OP_CONFIG_NO_SOLUTION = 113; //choix configuration dificile
   
    //Messages d'opérations affichage partie
    
    public static final int LISTE_PLAY = 139; //renvoyé la liste des parties jouées pour joueur anonyme
    public static final int LISTE_SESSION_DATE = 140; //renvoyé la liste des sessions avec leurs dates
    public static final int LISTE_PLAY_DATE = 141; //renvoyé la liste des parties jouées à une date précise
    public static final int LISTE_PLAY_CONF1 = 142; //renvoyé la liste des parties jouées ayant la configuration 1
    public static final int LISTE_PLAY_CONF2 = 143; //renvoyé la liste des parties jouées ayant la configuration 2
    public static final int LISTE_PLAY_CONF3 = 144; //renvoyé la liste des parties jouées ayant la configuration 3
    public static final int LISTE_PLAY_CONF4 = 145; //renvoyé la liste des parties jouées ayant la configuration 4
    public static final int LISTE_PLAY_CONF5 = 146; //renvoyé la liste des parties jouées ayant la configuration 5
    public static final int LISTE_PLAY_CONF6 = 147; //renvoyé la liste des parties jouées ayant la configuration 6
    
    //Messages d'opérations message d'erreur 
    
    public static final int TIMEOUT = 50; //temps de jeu depassé
    public static final int MAUVAISE_PLAQUE = 60; //mauvaise plaque
    public static final int ERREUR_EXPRESSION = 70; //erreur expression
    public static final int MAUVAIS_JEU = 0; //jeu inexistant
    public static final int MAUVAIS_JOEUR = -1; //joeur inexistant
    public static final int SESSION_FERME = 200; //lors que le joueur n'existe pas ou suprimer de la liste
    public static final int JOUEUR_EXISTANT = 210 ;// lors que le joueur veut s'inscrire avec un pseudo existant
    public static final int MAUVAIS_MDP = 211 ;// mauvais mot de passe
    
    //Messages d'opérations message solution
    
    public static final int BEST_SOLUTION = 150; //envoyer meilleurs solutions d'une partie
    

    public Message(Integer code, Object ob) {
        this.code = code;
        this.o = ob;
      
    }
    
    public Message(Object o)
    {
        if(o instanceof Message)
        {
            Message m = (Message)o;
            this.code = m.getCode();
            this.o = m.o;
        }
    }

    public Integer getCode() {
        return code;
    }
    
    public Object getObject()
    {
        return this.o;
    }

}
