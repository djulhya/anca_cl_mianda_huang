/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur;

import commun.ExceptionJeu;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import model.*;
import vue.Vue;

/**
 *
 * @author djulhya
 */

public class CtrlClient {
    
    private Vue vue;
    private AppliClient apClient;
    private Play play=null;
    private static String adrIP = "localhost";
    private static int port = 8189;
    private Timer t;
    boolean finJeu = false;
    private int numJoueur = -1;
    private Config config;
    private long duree=0;
    private Joueur joueur = new Joueur();
    
    public CtrlClient(String[] args){
        switch (args.length) {
            case 1:
                adrIP = args[0];
                break;
            case 2:
                adrIP = args[0];
                port = Integer.valueOf(args[1].trim()).intValue();
                break;
        }
        vue = new Vue();
        apClient = new AppliClient(adrIP, port);
        play = new Play();
        config = Config.values()[0];
    }
    
    
    
    public void recevoirButPropose()
    {
        Integer x = -1;
        boolean res = false;
        while(res == false)
        {
            try{
                x=vue.recevoirBut();
                if(x>0)
                    res = true;

                else
                {
                    vue.aff("le but proposé n'est pas correct, "
                            + "entrez un entier positif");
                }   
            }
            catch(Exception e)
            {
                this.affInit(2);
            
            }
        }
        
        this.play.setButPropose(x);
    }
    
    private void FinJeu()
    {
        //signaler la fin du jeu au serveur
       try{
           Resultat r =
                   new Resultat(this.play.getSolution(),
                   this.play.getButPropose() ,this.play.getButInt(), 
                   this.play.getTimeStart() , this.joueur);
          
           int s = this.apClient.finJeu(r);
           if(s==0)
               vue.aff("Désolé , vous avez perdu");
           else if(s>0)
               vue.aff("Bravo, vous avez gagné");
           if(s > -1)
                vue.aff("votre score est : "+s);
           vue.setFin(true);
       }
       catch(ExceptionJeu e){
           System.out.println(e.toString());}
     
    }
    
    private void quitterJeu(){
        try{
            this.apClient.quitterJeu(this.joueur);
        }
        catch(ExceptionJeu e){}
        t.purge();
        t.cancel();
        vue.quitterJeu();
        System.exit(0);
    }
     
    public boolean recevoirExpression(String str)
    {
         boolean res = true;
        int opg = 0, opd = 0, total = 0;
        String operation = "" ;
         
        //transformer le string vers objet expression
        String strOp[]=str.split("\\p{Punct}",3);
        try
        {
            opg=Integer.parseInt(strOp[0]);
            opd=Integer.parseInt(strOp[1]);
            total=Integer.parseInt(strOp[2]);
            operation=str.substring(strOp[0].length(),strOp[0].length()+1);
            
            this.play.ajouterExpression(opg, operation, opd, total); 
        }
        catch(Exception e)
        {
            vue.aff("Erreur expression");;
            res = false;
        } 
        //end transformation
        if(this.verifierPlaque(opg, opd)==true)
        {
                this.verifierExpression(opg, operation, opd, total);
        }
        else
        {
            vue.aff("mauvais choix de plaques");
            res = false;
        }
        
        return res;
    }
        
        
    private boolean verifierPlaque(int x , int y)
    {
        boolean res = false;
        if(x == y)
        {
            if(this.play.verifierPlaque(x))
            {
                if(this.play.doublePlaque(x))
                    res = true;
                else
                    res = false;
            }
        }
        else
        {
            if(!this.play.verifierPlaque(x)||!this.play.verifierPlaque(y))
            res=false;
            else
                res = true;
        }
      
        return res;
    }
    
    
   private boolean verifierExpression(int gauche , String operation, 
           int droit , int total)
   {
       boolean res = false;
       Operation op = Operation.addition ;
        if(operation.equals("+"))
        {
            res = this.play.verifierEprssion(gauche, droit, 
                    Operation.addition, total);
            op = Operation.addition;
        }
        else if(operation.equals("-"))
        {
            res = this.play.verifierEprssion(gauche, droit, 
                    Operation.soustraction, total);
            op = Operation.soustraction;
        }
        else if(operation.equals("*"))
        {
            res = this.play.verifierEprssion(gauche, droit, 
                    Operation.multiplication, total);
            op = Operation.multiplication;
        }
        else if(operation.equals("/"))
        {
            res = this.play.verifierEprssion(gauche, droit, 
                    Operation.division, total);
            op = Operation.division;
        }
               
        if(res == true)
        {
            this.play.supprimerPlaque(gauche);
            this.play.supprimerPlaque(droit);
            this.play.ajouterPlaque(this.play.getTotal());
        }
       
       return res;
   }
   
   
    private void boucleDeJeux()
    {
         int cpt = 0;
         String str ="";
         String msg ="OK";
         boolean continuer = true;
         while(continuer == true && cpt<5)
         {
            vue.aff(this.play.getlistPlaque().toString());
            System.out.println("donnez l'expression : entier1 "
                    + "operation entier2 = resultat ");
            if(!this.recevoirExpression(vue.recevoirString())
                    ||this.play.verifierResultat()||cpt==4)
            {
                t.purge();
                t.cancel();
                this.FinJeu();
                continuer = false;
            }
            ++cpt;
         }
         this.affInit(2);
    }
    
    
    private void configuration(int conf)
    {
        int reponse=0;
        try{
            if(this.joueur.getNumJoueur() == -1)
            {
                this.joueur.setNumJoueur(this.apClient.creerJoeur(conf));
                System.out.println("num joueur créé : "+this.joueur.getNumJoueur());
            }
            else
            {
                reponse= this.apClient.setConfiguration(conf , this.joueur);
                if(reponse==-1)
                {
                    this.joueur= new Joueur();
                    this.affInit(1);
                }
                else   
                    this.config=Config.values()[conf];
            }
        }
        catch(ExceptionJeu e){
            
            System.out.println(e.toString()); 
            System.out.println("erreur connexion serveur");
            System.exit(0);
        }
    }
    
    private void nouveauJeu()
    {
        Play input=null;
        try{
            
            Object o= this.apClient.lancerJeu(this.joueur);
            if(o instanceof Play ){
                input=(Play)o;
            }
            else{
                if(o instanceof Integer){
                    Integer i= (Integer)o;
                    if(i==-1){
                        
                        this.affInit(1);
                    }
                
                }
                
            }
            
        }
        catch(ExceptionJeu e){
            System.out.println(e.toString()); 
            System.out.println("erreur connexion serveur");
            System.exit(0);
        }
        if(input!=null)
        {
            t=new Timer();
            
            t.schedule(new TimerTask(){
                @Override
                public void run() {
                   System.out.println("Time's up!");
                   FinJeu();
                   vue.aff("tapez sur une touche + enter pour continuer");
                   t.cancel(); //Terminate the timer thread
                }
            },input.getDuree());
            this.play.nouvellePartie(input.getButB(),input.getPlaque()
                    ,input.getTimeStart());
            //prq nouvlle partie a un parametre But but non int but?? 
            //pour finalement donner un valeur int dans but de partie.
            vue.aff(Integer.toString(this.play.getButInt()));
            vue.aff(this.play.getlistPlaque().toString());
        }
        
    }
    
    public void choixDebutJeu(Integer choix)
    {
        switch (choix) {
            case 1:
                //demander but plaque
                this.nouveauJeu();
                vue.aff("vous avez 3 min pour joueur");
                vue.menuDebutJeux();
                this.choixClient(vue.recevoirChoix());
                break;
            case 2:
                vue.menuChoixConfiguration();
                this.configuration(vue.recevoirConfiguration());
                this.nouveauJeu();
                vue.aff("vous avez 3 min pour joueur");
                vue.menuDebutJeux();
                this.choixClient(vue.recevoirChoix());
                break;
            
            case 3:
                try{
                    Object o = this.apClient.afficherPlay(this.joueur);
                    if(o instanceof Integer)
                    { 
                        Integer i= (Integer)o;
                        if(i==-1){
                        this.affInit(1);
                        }
                    }
                    else{
                    this.afficherJeux((Stack<Play>)o);
                    }
                }
                catch(Exception e)
                {
                    vue.aff("liste de partie vide");
                }
                this.affInit(2);
                break;
            
            case 4:
                try{
                    Object o = this.apClient.afficherBestSolution(this.joueur);
                    if(o instanceof Integer)
                    { 
                        Integer i= (Integer)o;
                        if(i==-1){
                        this.affInit(1);
                        }
                    }
                    else{
                    this.afficheBSolution((ArrayList<Solution>)o);
                    }
                }
                catch(Exception e)
                {
                    vue.aff("liste de partie vide");
                    
                }
                this.affInit(2);
            case 5:
                if(this.inscription()!=-1)
                {
                    vue.aff("vous etes bien inscrit");
                    this.affInit(2);
                }
                else
                {
                    vue.aff("ce pseudo existe deja");
                }
                break;
            case 6:
                this.quitterJeu();
                break;
            
            default:
                vue.aff("Mauvais choix");
                this.affInit(2);
                break;
        }
    }
    
    public void choixInitial(Integer choix) 
    {
        switch (choix) {
            case 1:
                this.configuration(0);
                this.nouveauJeu();
                vue.aff("vous avez 3 min pour joueur");
                vue.menuDebutJeux();
                this.choixClient(vue.recevoirChoix());
                break;
            case 2:
                this.choixDebutJeu(2);
                break;
            case 3:
                try{
                    Object o = this.apClient.afficherPlay(this.joueur);
                    if(o instanceof Integer)
                    { 
                        Integer i= (Integer)o;
                        if(i==-1){
                        this.affInit(1);
                        }
                    }
                    else{
                    this.afficherJeux((Stack<Play>)o);
                    }
                }
                catch(Exception e)
                {
                    vue.aff("liste de partie vide "+e.getMessage());
                    
                }
                this.affInit(1);
                break;
                
            case 4:
                try{
                    Object o = this.apClient.afficherBestSolution(this.joueur);
                    if(o instanceof Integer)
                    { 
                        Integer i= (Integer)o;
                        if(i==-1){
                        this.affInit(1);
                        }
                    }
                    else{
                        this.afficheBSolution((List<Solution>)o);
                    }
                }
                catch(Exception e)
                {
                    vue.aff("liste de jeu vide");
                    
                }
                this.affInit(1);
                break;
             case 5:
                if(this.inscription()!=-1)
                {
                    vue.aff("vous etes bien inscrit");
                    this.affInit(2);
                }
                else
                {
                    vue.aff("ce pseudo existe deja");
                }
                break;
            case 6:
                System.exit(0); 
                break;
            
            default:
                vue.aff("Mauvais choix");
                this.affInit(1);
                break;
               
        }
    }

    public void choixClient(Integer choix) 
    {
        switch (choix)
        {
            case 1 :
                this.recevoirButPropose();
                this.boucleDeJeux();
                break;
            case 2 :
                t.purge();
                t.cancel();
                this.choixDebutJeu(1);
                break;
            case 3:
                t.purge();
                t.cancel();
                this.choixDebutJeu(2);
                break;
            case 4:
                this.quitterJeu();
                break;
            case -20:
                this.affInit(2);
                break;
             default:
                vue.aff("Mauvais choix");
                this.choixClient(vue.recevoirChoix());
                break;   
        }
             
        
    }
    
    
    //CHOIX JOUEUR CONNECTE
    
     public void choixDebutJeuConnexion(Integer choix)
    {
        switch (choix) {
            case 1:
                //demander but plaque
                this.nouveauJeu();
                vue.aff("vous avez 3 min pour joueur");
                vue.menuDebutJeux();
                this.choixClientConnexion(vue.recevoirChoix());
                break;
            case 2:
                vue.menuChoixConfiguration();
                this.configuration(vue.recevoirConfiguration());
                this.nouveauJeu();
                vue.aff("vous avez 3 min pour joueur");
                vue.menuDebutJeux();
                this.choixClientConnexion(vue.recevoirChoix());
                break;
            
            case 3:
                vue.menuAffichePartie();
                this.choixAffichePartie(vue.recevoirChoix());
                this.affInit(2);
                break;
            
            case 4:
                try{
                    Object o = this.apClient.afficherBestSolution(this.joueur);
                    if(o instanceof Integer)
                    { 
                        Integer i= (Integer)o;
                        if(i==-1){
                        this.joueur = new Joueur();
                        this.affInit(1);
                        }
                    }
                    else{
                        this.afficheBSolution((ArrayList<Solution>)o);
                    }
                }
                catch(Exception e)
                {
                    vue.aff("liste de partie vide");
                    
                }
                this.affInit(2);
                
            case 5:
                this.quitterJeu();
                break;
            
            default:
                vue.aff("Mauvais choix");
                this.affInit(2);
                break;
        }
    }
    
    public void choixInitialConnexion(Integer choix) 
    {
        switch (choix) {
            case 1:
                this.configuration(0);
                this.nouveauJeu();
                vue.aff("vous avez 3 min pour joueur");
                vue.menuDebutJeux();
                this.choixClient(vue.recevoirChoix());
                break;
            case 2:
                this.choixDebutJeu(2);
                break;
            case 3:
                vue.menuAffichePartie();
                this.choixAffichePartie(vue.recevoirChoix());
                this.affInit(1);
                break;
            case 4:
                try{
                    Object o = this.apClient.afficherBestSolution(this.joueur);
                    if(o instanceof Integer)
                    { 
                        Integer i= (Integer)o;
                        if(i==-1){
                        this.numJoueur=-1;
                        this.affInit(1);
                        }
                    }
                    else{
                        this.afficheBSolution((List<Solution>)o);
                    }
                }
                catch(Exception e)
                {
                    vue.aff("liste de jeu vide");
                    
                }
                this.affInit(1);
                break;
                
            case 5:
                System.exit(0); 
                break;
            
            default:
                vue.aff("Mauvais choix");
                this.affInit(1);
                break;
               
        }
    }

    public void choixClientConnexion(Integer choix) 
    {
        switch (choix)
        {
            case 1 :
                this.recevoirButPropose();
                this.boucleDeJeux();
                break;
            case 2 :
                t.purge();
                t.cancel();
                this.choixDebutJeu(1);
                break;
            case 3:
                t.purge();
                t.cancel();
                this.choixDebutJeu(2);
                break;
            case 4:
                this.quitterJeu();
                break;
            case -20:
                this.affInit(2);
                break;
             default:
                vue.aff("Mauvais choix");
                this.choixClient(vue.recevoirChoix());
                break;   
        }
             
        
    }
    
    public void choixAffichePartie(Integer choix)
    {
        switch (choix)
        {
            case 1 :
                try
                {
                    Object o = this.apClient.afficherSession(this.joueur);
                    if(o instanceof Integer)
                    { 
                        Integer i= (Integer)o;
                        if(i==-1)
                        {
                            vue.aff("la session est fermé. veuillez vous reconnecter");
                            this.joueur = new Joueur();
                            this.affConnexion();
                        }
                    }
                    else
                    {
                        if(this.afficheListeString(o)>0)
                        {
                            vue.aff("entrez le numéro de session que vous voulez");
                            this.joueur.msg = vue.recevoirChoix();
                            try
                            {
                                Object ob = this.apClient.afficherPlayDate(joueur);
                                if(o instanceof Integer)
                                { 
                                        Integer i= (Integer)ob;
                                    if(i==-1)
                                   {
                                            vue.aff("votre session est fermé. veuillez vous reconnecter");
                                            this.joueur = new Joueur();
                                            this.affConnexion();
                                        }
                                    }
                                else
                                {
                                    this.affichePlayConnecte(ob);
                                }

                            }
                            catch(Exception e)
                            {
                                vue.aff("la liste est vide");
                            }
                        }
                        else
                            vue.aff("vous n'avait pas encore joué");
                    
                    }
                   
                }
                catch(Exception e)
                {
                    vue.aff("pas de session enregistrée");
                }
                
                break;
            case 2://selon les configurations
                vue.menuAffConfig();
                int x=vue.recevoirChoix();
                try
                {
                    Object o = this.apClient.afficherPlayConf(x, this.joueur);
                    if(o instanceof Integer)
                    { 
                        Integer i= (Integer)o;
                        if(i==-1)
                        {
                            vue.aff("la session est fermé. veuillez vous reconnecter");
                            this.joueur = new Joueur();
                            this.affConnexion();
                        }
                    }
                    else{
                        this.affichePlayConnecte(o);
                    }
                    
                }
                catch(Exception e)
                {
                    vue.aff("la liste est vide");
                }
                break;
            
            case 4 :
                this.quitterJeu();
                break;
            default:
                break;
        }
    }
    
    
    //-------------------------------------------------------------/
    
    public void choixConnexion(Integer choix)
    {
        switch (choix)
        {
            case 1 :
                this.affInit(1);//joueur anonymement
                break;
            case 2 : //se connecter
                if(this.connexion()!=-1)
                {
                    this.affInit(1);
                }
                else
                {
                    vue.aff("mauvais pseudo et/ou mot de passe");
                    this.affConnexion();
                }
                break;
            case 3 : //s'inscrire
                if(this.inscription()!=-1)
                {
                    System.out.println("attente affinit1");
                    this.affInit(1);
                }
                else
                {
                    vue.aff("ce pseudo existe deja");
                    this.affConnexion();
                }
                break;
            case 4:
                this.quitterJeu();
                break;
            default :
                vue.aff("mauvais chois");
                this.affConnexion();
                break;
        }
    }
    
    public void affConnexion()
    {
        vue.menuConnexion();
        this.choixConnexion(vue.recevoirChoix());
        // affInit(1);
    }
    public void affInit(int x) 
    {
        vue.setFin(false);
        if(this.joueur.getIdJoueur()=="")
        {
            vue.menuInitial();
            if(x==1)
                this.choixInitial(vue.recevoirChoix());
            else if(x==2)
                this.choixDebutJeu(vue.recevoirChoix());
        }
        else
        {
            vue.menuInitialConnexion();
            if(x==1)
                this.choixInitialConnexion(vue.recevoirChoix());
            else if(x==2)
                this.choixDebutJeuConnexion(vue.recevoirChoix());
        }
     } 

    
    public void afficherJeux(Stack<Play> j)
    {
        try{
           vue.aff("***************************************************");
            for(Play p : j)
            {
                if(p.getJouer().equals("ok"))
                    p.afficherPlay();
                vue.aff("***************************************************");
            }
        }
        catch(Exception e)
        {
            vue.aff("liste de jeu vide");
        }
    }
    
    private void afficheBSolution(List<Solution> arrayList) {
        try{
            vue.aff("***************************************************");
            for(Solution s:arrayList)
            {
                System.out.println(s.toString());
                vue.aff("***************************************************");
            }
        }
        catch(Exception e)
        {
            vue.aff("liste de jeu vide" + e.toString());
        }
    }
    
    private int afficheListeString(Object s)
    {
        List<String> session = (List<String>) s;
        try{
           
            vue.aff("***************************************************");
            for(String st: session)
            {
                System.out.println(st.toString());
                vue.aff("***************************************************");
            }
        }
        catch(Exception e)
        {
            vue.aff("liste des sessions est vide");
        }
        
        return session.size();
    }
   
    private void affichePlayConnecte(Object o)
    {
        //à faire
        if(this.afficheListeString(o)==0)
            vue.aff("la liste des jeux est vide");
    }
    private int inscription()
    {
        String pseudo="";
        String mdp="";
        vue.aff("entrez votre pseudo : ");
        pseudo = vue.recevoirString();
        vue.aff("entrez votre mot de passe");
        mdp = vue.recevoirString();
        
        //Joueur j = new Joueur(pseudo,mdp);
        int x = -1;
        try
        {
            int oldnum = this.joueur.getNumJoueur();
            
            this.joueur.setIdJoueur(pseudo);
            this.joueur.setMotDePasse(mdp);
            
            x = this.apClient.inscrireJoueur(this.joueur);
            if(x!=-1)
            {
                this.joueur.setNumJoueur(x);
            }
            else
            {
                this.joueur = new Joueur();
                this.joueur.setNumJoueur(oldnum);
                
            }
        }
        catch(Exception e)
        {
        }
        return x;
    }
    
    
    private int connexion()
    {
        String pseudo="";
        String mdp="";
        vue.aff("entrez votre pseudo : ");
        pseudo = vue.recevoirString();
        vue.aff("entrez votre mot de passe");
        mdp = vue.recevoirString();
        
        Joueur j = new Joueur(pseudo,mdp);
        int x = -1;
        try
        {
            x = this.apClient.connecterJoueur(j);
            if(x!=-1)
            {
                this.joueur.setIdJoueur(pseudo);
                this.joueur.setMotDePasse(mdp);
                this.joueur.setNumJoueur(x);
            }
        }
        catch(Exception e)
        {
        }
        
        return x;
    }
}
