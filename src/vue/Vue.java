/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;
import java.util.Scanner;

/**
 *
 * @author djulhya
 */
public class Vue {
    boolean fin = false;
   
    public Vue()
    {
    }

    public void menuConnexion()
    {
         System.out.println("--------------------------------------------"
                + "-------------------------------------");
         
         System.out.println("Tapez 1 pour Jouer anonymement ");
         System.out.println("Tapez 2 pour se connecter ");
         System.out.println("Tapez 3 pour s'inscrire ");
         System.out.println("Tapez 4 pour Quitter");
         
         System.out.println("--------------------------------------------"
                + "-------------------------------------");
    }
    
    public void menuInitial() {
        System.out.println("--------------------------------------------"
                + "-------------------------------------");
        System.out.println();
        System.out.println("Tapez 1 pour Jouer ");
        System.out.println("Tapez 2 pour choisir une configuration et joeur ");
        System.out.println("Tapez 3 pour afficher les parties jouées précédemment ");
        System.out.println("Tapez 4 pour afficher les meilleurs solutions");
        System.out.println("Tapez 5 pour s'inscrire");
        System.out.println("Tapez 6 pour Quitter");
        System.out.println();
        System.out.println("------------------------------------------------"
                + "---------------------------------");
       
    }
 
    
    public void menuChoixConfiguration()
    {
        System.out.println("------------------------------------------------"
                + "---------------------------------");
        
        System.out.println("Tapez 0 pour un niveau par defaut :3 min , "
                + "plaques et but aléatoires");
        System.out.println("Tapez 1 pour un niveau facile , longueur solution <=2");
        System.out.println("Tapez 2 pour un niveau moyen , longueur solution = 3 ou 4");
        System.out.println("Tapez 3 pour un niveau difficile , longueur solution = 5");
        System.out.println("Tapez 4 pour un niveau sans solution exacte ");
        System.out.println("Tapez 5 pour un niveau avec solution exacte ");
        System.out.println("------------------------------------------------"
                + "---------------------------------");
    }
    
    public void menuDebutJeux()
    {
     
        System.out.println("------------------------------------------------"
                + "---------------------------------");
        System.out.println();
        System.out.println("Tapez 1 pour annonce le but  ");
        System.out.println("Tapez 2 pour lancer une nouvelle partie");
        System.out.println("Tapez 3 pour changer de configuration et jouer ");
        System.out.println("Tapez 4 pour quitter");
        System.out.println();
        System.out.println("-----------------------------------------------"
                + "----------------------------------");
    }
    
    
    
    
    //MENU JOUEUR CONNECTE
    
    public void menuInitialConnexion() {
        System.out.println("--------------------------------------------"
                + "-------------------------------------");
        System.out.println();
        System.out.println("Tapez 1 pour Jouer ");
        System.out.println("Tapez 2 pour choisir une configuration et joeur ");
        System.out.println("Tapez 3 pour afficher les parties jouées précédemment ");
        System.out.println("Tapez 4 pour afficher les meilleurs solutions");
        System.out.println("Tapez 5 pour Quitter");
        System.out.println();
        System.out.println("------------------------------------------------"
                + "---------------------------------");
       
    }
 
   public void menuAffichePartie()
   {
       System.out.println("--------------------------------------------"
                + "-------------------------------------");
        System.out.println();
        System.out.println("Tapez 1 pour afficher les parties d'une certaine date ");
        System.out.println("Tapez 2 pour afficher les parties d'une certaine configuration ");
        System.out.println("Tapez 3 pour revenir au menu du jeu");
        System.out.println("Tapez 4 pour quitter");
        System.out.println();
        System.out.println("------------------------------------------------"
                + "---------------------------------");
   }
    
    public void menuAffConfig()
    {
        System.out.println("--------------------------------------------"
                + "-------------------------------------");
        System.out.println();
        System.out.println("Tapez 1 pour la configuration par defaut ");
        System.out.println("Tapez 2 pour la configuration facile ");
        System.out.println("Tapez 3 pour la configuration moyenne");
        System.out.println("Tapez 4 pour la configuration difficile ");
        System.out.println("Tapez 5 pour la configuration avec solution exacte ");
        System.out.println("Tapez 6 pour la configuration sans solution");
        System.out.println("Tapez 7 pour quitter");
        System.out.println();
        System.out.println("------------------------------------------------"
                + "---------------------------------");
    }
   
    public int recevoirChoix()
    {
        int x = -1;
         Scanner s = new Scanner(System.in);
            try
            {
                int i = s.nextInt();
                if(fin == false)
                    x = i;
                else
                    x = -20;
            }
            catch(Exception e)
            {
                System.out.println("Entrez un entier");
            }
            
          return x; 
    }
    
    public String recevoirString()
    {
        String str ="";
        
        Scanner s = new Scanner(System.in);
        str = s.nextLine();
        
        return str;
    }
    
    public Integer recevoirBut()
    {
        System.out.println("donnez le but");
        
        Integer x = -1;
        
            Scanner s = new Scanner(System.in);
            try
            {                
                int i = s.nextInt();
                if(fin == false)
                    x = i;
                else
                    x = null;
            }
            catch(Exception e){}
          
        return x;
    }
    
    public int recevoirConfiguration()
    {
        int x = 0;
        Scanner s = new Scanner(System.in);
        try
        {
            x = s.nextInt();
        }
        catch(Exception e)
        {
            System.out.println("vous aurez la configuration par defaut");
        }
        
        if(x<0 || x>5)
            x=0;
        return x;
        
    }
     public void aff(String msg) {
        System.out.println(msg);
    }
      public void quitterJeu()
    {
        if(fin==false)
        {
            System.out.println("----------Au revoir----------");
            fin = true;
        }
    }
      
      public void setFin(boolean b)
      {
          this.fin = b;
      }
    
}
