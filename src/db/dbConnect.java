/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Play;

/**
 *
 * @author xun
 */
public class dbConnect {
    
     // JDBC driver name and database URL
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://localhost/compte_est_bon";
   
   //  Database credentials
   static final String USER = "root";
   static final String PASS = "";
   
    Connection con = null;
    
    public dbConnect(){
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void connexion() {
        if (con != null) {
            return;
        }
        try {
            con = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deconnexion() {
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    public boolean addJoueur(String log, String pwd) {

        try {
            Statement statement = con.createStatement();
            String sql = "INSERT INTO joueur(login,pwd) VALUES ('" + log + "','"+ pwd +"')";
            statement.executeUpdate(sql);
            
        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public boolean existJoueur(String log, String pwd){
        boolean b=false;
        try {
            Statement statement = con.createStatement();
            String sql = "SELECT * FROM joueur WHERE login = '"+ log +"'AND `pwd`= '"+ pwd +"'";
            ResultSet result = statement.executeQuery(sql);
            if (result.next()) {
                b=true;
            }
            else{ b=false;}

        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            b= false;
        }
        return b;
    } 
    
    public boolean existLog(String log){
        boolean b=false;
        try {
            Statement statement = con.createStatement();
            String sql = "SELECT * FROM joueur WHERE login = '"+ log +"'";
            ResultSet result = statement.executeQuery(sql);
            if (result.next()) {
                b=true;
            }
            else{ b=false;}

        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            b= false;
        }
        return b;
    }
    
    public boolean existPwd(String pwd){
        boolean b=false;
        try {
            Statement statement = con.createStatement();
            String sql = "SELECT * FROM joueur WHERE pwd = '"+ pwd +"'";
            ResultSet result = statement.executeQuery(sql);
            if (result.next()) {
                b=true;
            }
            else{ b=false;}

        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            b= false;
        }
        return b;
    }
    
    
    public boolean existSession(String log){
        boolean b=false;
        try {
            Statement statement = con.createStatement();
            String sql = "SELECT idSession FROM SESSION WHERE session.login = '"+log+"'";
            ResultSet result = statement.executeQuery(sql);
            if(result.next()) {
                b=true;
            }
            else{ b=false;}

        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            b= false;
        }
        return b;
    }
    
    public boolean addSession(String log) {

        try {
            Statement statement = con.createStatement();
            String sql = "INSERT INTO session(date,login) VALUES (now(),'"+ log +"')";
            statement.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public int getLastIdSession(String log)
    {
        int x = -1;
        try {
            Statement statement = con.createStatement();
            String sql = "SELECT idSession FROM SESSION WHERE session.login = '"+log+"'";
            ResultSet result = statement.executeQuery(sql);
            while(result.next()) {
                x= result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return x;
    }
    
    public boolean existPartie(int idP){
        boolean b=false;
        try {
            Statement statement = con.createStatement();
            String sql = "SELECT * FROM partie WHERE idPartie = '"+ idP +"'";
            ResultSet result = statement.executeQuery(sql);
            if(result.next()) {
                b=true;
            }
            else{ b=false;}

        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            b= false;
        }
        return b;
    }
    
    public boolean addPartie(int idP,int numSession,int numConfig, int but ,int annonce,
            int plaque1, int plaque2, int plaque3, int plaque4, int plaque5, int plaque6,
            String expression1,String expression2,String expression3,String expression4,
            String expression5) {

        try {
            Statement statement = con.createStatement();
            String sql = "INSERT INTO partie(idPartie,numSession,numConfig,but,"
                    + "annonce,plaque1,plaque2,plaque3,plaque4,plaque5,plaque6,"
                    + "expression1,expression2,expression3,expression4,expression5) "
                    + "VALUES ('" + idP + "','"+ numSession +"','"+ numConfig 
                    +"','"+ but +"','"+ annonce +"','"+ plaque1 + "','"+ 
                    plaque2 +"','"+ plaque3 + "','"+ plaque4 + "','"+ plaque5 +
                    "','"+ plaque6 + "','"+ expression1 + "','"+ expression2 + "','"+ expression3 +
                    "','"+ expression4 +"','"+ expression5 +"')";
            statement.executeUpdate(sql);
            System.out.println(sql);
        } catch (SQLException ex) {
            System.out.println("probleme dans add partie => insert");
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public boolean addPartie (Play p, int numSession){
        
        //getPlaques
        int plaque1=p.getPlaque().getListPlaqueSelect().get(0).getNombre();
        int plaque2=p.getPlaque().getListPlaqueSelect().get(1).getNombre();
        int plaque3=p.getPlaque().getListPlaqueSelect().get(2).getNombre();
        int plaque4=p.getPlaque().getListPlaqueSelect().get(3).getNombre();
        int plaque5=p.getPlaque().getListPlaqueSelect().get(4).getNombre();
        int plaque6=p.getPlaque().getListPlaqueSelect().get(5).getNombre();
        
        //get expression
        String expression1="";
        String expression2="";
        String expression3="";
        String expression4="";
        String expression5="";
        
        for(int x=0 ; x<p.getSolution().getListExpression().size();x++)
        {
           switch (x)
           {
                case 0:
                    expression1 = p.getSolution().getListExpression().get(x).toString();
                    break;
                case 1 :
                    expression2 = p.getSolution().getListExpression().get(x).toString();
                    break;
                case 2 :
                    expression3 = p.getSolution().getListExpression().get(x).toString();
                    break;
                case 3 :
                    expression4 = p.getSolution().getListExpression().get(x).toString();
                    break;
                case 4 :
                    expression5 = p.getSolution().getListExpression().get(x).toString();
                    break;
                
           }
        }
        
        return addPartie(p.getNumJeu(),numSession,p.getConfig(), p.getButInt(),p.getButPropose(),
            plaque1, plaque2, plaque3, plaque4, plaque5, plaque6,
            expression1,expression2, expression3, expression4, expression5);
        
    }
    
    public ArrayList<String> affParties(String log, String date){
        ArrayList<String> res = new ArrayList<>();

        int cpt = 0;
        try {
            String sql="";
            Statement statement = con.createStatement();
            sql= "SELECT * FROM partie WHERE numSession IN "
                    + "(SELECT idSession FROM SESSION WHERE session.login = '"+log+"')";
            if(date !=""){
            sql += " AND date = '"+date+"'";
            }
            sql += ")";
            System.out.println(sql);
            ResultSet result = statement.executeQuery(sql);
            while(result.next()) {
                ++cpt;
                String affPartie="";
                String expression1 = result.getString("expression1");
                String expression2 = result.getString("expression2");
                String expression3 = result.getString("expression3");
                String expression4 = result.getString("expression4");
                String expression5 = result.getString("expression5");
                
                affPartie += "la partie n°"+result.getInt("idPartie")
                        + " de la session n°"+result.getInt("numSession")+"\n";
                affPartie += "la configuration n°"+result.getInt("numConfig")+"\n";
                affPartie += "le but "+result.getInt("but")+" - " + "l'annonce "+result.getInt("annonce")+"\n";
                affPartie += "les plaques : "+result.getInt("plaque1")+","
                        +result.getInt("plaque2")+","+result.getInt("plaque3")
                        +","+result.getInt("plaque4")+","
                        +result.getInt("plaque5")+","+result.getInt("plaque6")+"\n";
                affPartie += "les expressions: \n" + expression1+"\n";
                if (!expression2.equals("")){affPartie += expression2+"\n";}
                else if (!expression3.equals("")){affPartie += expression3+"\n";}
                else if (!expression4.equals("")){affPartie += expression4+"\n";}
                else if (!expression5.equals("")){affPartie += expression5+"\n";}
                res.add(affPartie);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            ;
        }
        System.out.println(cpt);
        return res;
    }

    public List<String> getPartiesConfig(String idJoueur, int config) {
        ArrayList<String> res = new ArrayList<String>();
        String sql="";
        
        int cpt = 0;
        try {
            Statement statement = con.createStatement();
            sql= "SELECT * FROM partie WHERE numConfig  = '"+config+"' AND numSession IN "
                    + "(SELECT idSession FROM SESSION WHERE session.login = '"+idJoueur+"')";
            System.out.println(sql);
            ResultSet result = statement.executeQuery(sql);
            while(result.next()) {
                ++cpt;
                String affPartie="";
                String expression1 = result.getString("expression1");
                String expression2 = result.getString("expression2");
                String expression3 = result.getString("expression3");
                String expression4 = result.getString("expression4");
                String expression5 = result.getString("expression5");
                
                affPartie += "la partie n°"+result.getInt("idPartie")
                        + " de la session n°"+result.getInt("numSession")+"\n";
                affPartie += "le but "+result.getInt("but")+" - " + "l'annonce "+result.getInt("annonce")+"\n";
                affPartie += "les plaques : "+result.getInt("plaque1")+","
                        +result.getInt("plaque2")+","+result.getInt("plaque3")
                        +","+result.getInt("plaque4")+","
                        +result.getInt("plaque5")+","+result.getInt("plaque6")+"\n";
                affPartie += "les expressions: \n" + expression1+"\n";
                if (!expression2.equals("")){affPartie += expression2+"\n";}
                else if (!expression3.equals("")){affPartie += expression3+"\n";}
                else if (!expression4.equals("")){affPartie += expression4+"\n";}
                else if (!expression5.equals("")){affPartie += expression5+"\n";}
                res.add(affPartie);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        System.out.println(cpt);
        return res;
    }

    public List<String> getPartiesIdSession(int idSession) {
        ArrayList<String> res = new ArrayList<String>();
        String sql="";
        
        int cpt = 0;
        try {
            Statement statement = con.createStatement();
            sql= "SELECT * FROM partie WHERE numSession = '"+idSession+"'";
            System.out.println(sql);
            ResultSet result = statement.executeQuery(sql);
            while(result.next()) {
                ++cpt;
                String affPartie="";
                String expression1 = result.getString("expression1");
                String expression2 = result.getString("expression2");
                String expression3 = result.getString("expression3");
                String expression4 = result.getString("expression4");
                String expression5 = result.getString("expression5");
                
                affPartie += "la partie n°"+result.getInt("idPartie")
                        + " de la session n°"+result.getInt("numSession")+"\n";
                affPartie += "le but "+result.getInt("but")+" - " + "l'annonce "+result.getInt("annonce")+"\n";
                affPartie += "les plaques : "+result.getInt("plaque1")+","
                        +result.getInt("plaque2")+","+result.getInt("plaque3")
                        +","+result.getInt("plaque4")+","
                        +result.getInt("plaque5")+","+result.getInt("plaque6")+"\n";
                affPartie += "les expressions: \n" + expression1+"\n";
                if (!expression2.equals("")){affPartie += expression2+"\n";}
                else if (!expression3.equals("")){affPartie += expression3+"\n";}
                else if (!expression4.equals("")){affPartie += expression4+"\n";}
                else if (!expression5.equals("")){affPartie += expression5+"\n";}
                res.add(affPartie);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        System.out.println(cpt);
        return res;
    }

    public List<String> getListPlaySessionDate(String idJoueur) {
       ArrayList<String> res = new ArrayList<String>();
        String sql="";
        
        int cpt = 0;
        try {
            Statement statement = con.createStatement();
            sql= "SELECT * FROM SESSION WHERE login = '"+idJoueur+"'";
            System.out.println(sql);
            ResultSet result = statement.executeQuery(sql);
            while(result.next()) {
                ++cpt;
                String affPartie="";
                affPartie += "la session n°"+result.getInt("idSession")+" daté du "+result.getString("date")+"\n";
                
                res.add(affPartie);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        System.out.println(cpt);
        return res;
    
    }
}
