/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package protocol;

import commun.Message;
import commun.Protocol;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ProtocolClient extends Protocol{
    
    int port;
    String adrIp;
    
    public ProtocolClient()
    {
        
    }

    public ProtocolClient(String adrIP, int port) {
        this.adrIp= adrIP;
        this.port=port;
      
    }
     
        public Message send_message(int code, Object o) {
        Message msgRep = null;
        if (connexionTCP() == CONN_OK) {
            int etat = connexionJeu();
            if (etat == CONN_ACK) {
                etat = envoiMsg(new Message(code, o));
                if (etat == CONN_ACK){
                    msgRep = attenteReponse();
                }
            }
            if (etat != CONN_KO)
                deconnexionTCP();
         }
        return msgRep;
    }

    private int connexionTCP() 
    {
        try {
            
            socket = new Socket(adrIP, port);
           
           this.inObject=new ObjectInputStream(socket.getInputStream());
           this.outObject=new ObjectOutputStream(socket.getOutputStream());
            return CONN_OK;
        } catch (Exception e) {
            return CONN_KO;
        }
    }

    private int connexionJeu() 
    {
        try {
            write("RQST");
            String s = this.inObject.readObject().toString();
             if(s.equals("ACK"))
                return CONN_ACK;
             else 
                return CONN_NOT_JEU;
             
        } catch (Exception ex) {
            return CONN_KO;
        }
    }

    private int envoiMsg(Message msgToSend) {
        write(msgToSend);
        
        try{
            String s = this.inObject.readObject().toString();
             if(s.equals("ACK"))
                return CONN_ACK;
             else 
                return CONN_NOT_JEU;
             
        } catch (Exception ex) {
            return CONN_KO;
        }
        
    }
    
    private Message attenteReponse() {
        try {
            
            Message m = (Message)inObject.readObject();
            
            msg_recu = new Message(m);
            write("ACK");
            return msg_recu;
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    private int deconnexionTCP() {
        try {
            socket.close();
            return CONN_OK;
        } catch (Exception e) {
            return CONN_KO;
        }
    }
}
